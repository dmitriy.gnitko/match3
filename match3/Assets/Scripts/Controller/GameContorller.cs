﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameContorller : MonoBehaviour
{
    public enum GameState
    {
        Undefined,
        GenerateRandomField,
        WaitUserMove,
        AnimUserMove,
        Shuffle,
    }

    public RectTransform grid;
    public CellView cellTemplate;

    private readonly Dictionary<Vector2Int, CellView> _cells = new Dictionary<Vector2Int, CellView>();
    private readonly List<CellView> _selectedCells = new List<CellView>();

    private GameState _currentGameState;

    #region ----- Unity methods -----

    void Start()
    {
        Clear();
        FillRandomField();
        Subscribe();
    }

    void OnDestroy()
    {
        UnSubscribe();
    }

    #endregion

    #region ----- Main logic -----
    
    private void Subscribe()
    {
        MainEventBusHandler.OnCellSelected -= OnCellSelected;
        MainEventBusHandler.OnCellSelected += OnCellSelected;
    }

    private void UnSubscribe()
    {
        MainEventBusHandler.OnCellSelected -= OnCellSelected;
    }

    private void SetState(GameState newState)
    {
        if (_currentGameState == newState) return;
        _currentGameState = newState;
    }

    private void Clear()
    {
        foreach (var cell in _cells.Values) {
            GameObject.Destroy(cell.gameObject);
        }
        _cells.Clear();
    }

    private void FillRandomField()
    {
        SetState(GameState.GenerateRandomField);

        List<Vector2Int> emptyCells = Utility.GetEmptyCells();

        Vector2Int place = new Vector2Int();
        for (int i = 0; i < LevelSettings.ROW_CNT; i++) {
            for (int j = 0; j < LevelSettings.COL_CNT; j++) {
                place.x = i;
                place.y = j;

                AddNewCellToPlace(place, emptyCells);
            }
        }

        MainEventBusHandler.DispachShowNotif("Новая игра!");

        SetState(GameState.WaitUserMove);
    }

    private void AddNewCellToPlace(Vector2Int place, List<Vector2Int> emptyCells)
    {
        var cell = GameObject.Instantiate<CellView>(cellTemplate);
        cell.transform.SetParent(grid.transform);
        cell.SetPlace(place, 0f);
        if (emptyCells.Contains(place) == false) {
            _cells.GetRandomColorForCell(cell);
        }
        else {
            cell.SetColor(LevelSettings.EMPTY_COLOR);
        }
        _cells[cell.GetPlace()] = cell;
    }

    private void OnCellSelected(CellView cell)
    {
        if (_currentGameState != GameState.WaitUserMove) return;

        if (_selectedCells.Contains(cell) == false) {
            _selectedCells.Add(cell);
            cell.SetSelected(true);
            if (_selectedCells.Count > 1) {

                var c1 = _selectedCells[0].GetPlace();
                var c2 = _selectedCells[1].GetPlace();
                if (c1.y == c2.y && (c1.x + 1 == c2.x || c1.x - 1 == c2.x)
                    || c1.x == c2.x && (c1.y + 1 == c2.y || c1.y - 1 == c2.y)) {
                    StarUserMoveAnimation();
                }
                else {
                    DeselectAllCells();
                    _selectedCells.Clear();

                    _selectedCells.Add(cell);
                    cell.SetSelected(true);
                }
            }
        }
        else {
            cell.SetSelected(false);
            _selectedCells.Clear();
        }
    }

    private void StarUserMoveAnimation()
    {
        SetState(GameState.AnimUserMove);

        StopCoroutine(AnimationMove());
        StartCoroutine(AnimationMove());
    }

    private IEnumerator AnimationMove()
    {
        yield return new WaitForSeconds(0.15f);

        DeselectAllCells();

        _selectedCells[0].MoveTo(_selectedCells[1].GetPlace(), LevelSettings.ANIM_SWAP_TIME);
        _selectedCells[1].MoveTo(_selectedCells[0].GetPlace(), LevelSettings.ANIM_SWAP_TIME);


        yield return new WaitForSeconds(LevelSettings.ANIM_SWAP_TIME);

        _cells.Swap(_selectedCells[0], _selectedCells[1]);

        yield return new WaitForSeconds(LevelSettings.ANIM_FALL_TIME);

        bool hasAnyMatch = _cells.CheckMatchingFor(_selectedCells, out var checkForMatching);

        if (checkForMatching.Count > 0) {
            StartCoroutine(ReCheck(checkForMatching));
        }else if (hasAnyMatch == false) {
            _selectedCells[0].MoveTo(_selectedCells[1].GetPlace(), LevelSettings.ANIM_SWAP_TIME);
            _selectedCells[1].MoveTo(_selectedCells[0].GetPlace(), LevelSettings.ANIM_SWAP_TIME);
            yield return new WaitForSeconds(LevelSettings.ANIM_SWAP_TIME);
            _cells.Swap(_selectedCells[0], _selectedCells[1]);
            SetState(GameState.WaitUserMove);
        }
        else {
            SetState(GameState.WaitUserMove);
        }

        _selectedCells.Clear();
    }
    
    private IEnumerator ReCheck(List<CellView> cellViews)
    {
        yield return new WaitForSeconds(LevelSettings.ANIM_FALL_TIME);
        if (_cells.CheckMatchingFor(cellViews, out var checkForMatching) == false) {
            if (_cells.HasPossibleMoves()) {
                SetState(GameState.WaitUserMove);
            }
            else {
                MainEventBusHandler.DispachShowNotif("Нет доступных ходов!\n Перемешиваем!");
                SetState(GameState.Shuffle);
                ShuffleField();
                SetState(GameState.WaitUserMove);
            }
        }
        else if (checkForMatching.Count > 0) {
            StartCoroutine(ReCheck(checkForMatching));
        }
        else {
            SetState(GameState.WaitUserMove);
        }
    }

    private void ShuffleField()
    {
        do {
            for (int i = 0; i < _cells.Count/2; i++) {
                int firstRandom = Random.Range(0, _cells.Count);
                int secondRandom = Random.Range(0, _cells.Count);
                Utility.SwapColor(_cells[_cells.Keys.ToList()[firstRandom]], _cells[_cells.Keys.ToList()[secondRandom]]);
            }
        } while (_cells.HasPossibleMoves() == false);
    }

    private void DeselectAllCells()
    {
        for (int i = 0; i < _selectedCells.Count; i++) {
            _selectedCells[i].SetSelected(false);
        }
    }

    #endregion

    #region ----- Buttons callback -----

    public void RestartGameBtnClickHandler()
    {
        MainEventBusHandler.DispachRestartGame();
        Clear();
        FillRandomField();
    }

    #endregion
}
