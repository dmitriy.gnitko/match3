﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSettings
{
    public static readonly int ROW_CNT = 6;
    public static readonly int COL_CNT = 6;
    public static readonly int EMPTY_CELL_CNT = 3;
    public static readonly float ANIM_SWAP_TIME = 0.5f;
    public static readonly float ANIM_FALL_TIME = 0.25f;
    public static readonly float NOTIF_ANIM_TIME = 0.5f;
    public static readonly float NOTIF_LIFE_TIME = 1f;
    public static readonly Color EMPTY_COLOR = Color.clear;
}
