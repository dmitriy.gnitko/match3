﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility
{

    private static readonly List<Color> _colorsTemplate = new List<Color>()
    {
        Color.green,
        Color.blue,
        Color.red,
        Color.yellow,
    };

    public static bool IsNullOrEmpty(this CellView value)
    {
        return value == null || value.IsEmpty();
    }

    public static bool HasAnyMatch(this Dictionary<Vector2Int, CellView> _cells, CellView cell, out List<CellView> matchingCells)
    {
        matchingCells = new List<CellView>();
        int rightMatch = 0;
        int leftMatch = 0;
        int upMatch = 0;
        int downMatch = 0;
        bool sameCell = true;
        CellView nextCell = null;
        Vector2Int nextCellPlace = cell.GetPlace();
        while (sameCell) {
            nextCellPlace.x--;
            if (_cells.TryGetValue(nextCellPlace, out nextCell) == false
                || nextCell.IsNullOrEmpty()
                || nextCell.GetColorAlias() != cell.GetColorAlias()) {
                break;
            }


            matchingCells.Add(nextCell);
            leftMatch++;
        }
        nextCellPlace = cell.GetPlace();
        while (sameCell) {
            nextCellPlace.x++;
            if (_cells.TryGetValue(nextCellPlace, out nextCell) == false
                || nextCell.IsNullOrEmpty()
                || nextCell.GetColorAlias() != cell.GetColorAlias()) {
                break;
            }

            matchingCells.Add(nextCell);
            rightMatch++;
        }

        if (leftMatch + rightMatch + 1 >= 3) {
            matchingCells.Add(cell);
            return true;
        }
        matchingCells.Clear();

        nextCellPlace = cell.GetPlace();
        while (sameCell) {
            nextCellPlace.y--;
            if (_cells.TryGetValue(nextCellPlace, out nextCell) == false
                || nextCell.IsNullOrEmpty()
                || nextCell.GetColorAlias() != cell.GetColorAlias()) {
                break;
            }

            matchingCells.Add(nextCell);
            upMatch++;
        }
        nextCellPlace = cell.GetPlace();
        while (sameCell) {
            nextCellPlace.y++;
            if (_cells.TryGetValue(nextCellPlace, out nextCell) == false
                || nextCell.IsNullOrEmpty()
                || nextCell.GetColorAlias() != cell.GetColorAlias()) {
                break;
            }

            matchingCells.Add(nextCell);
            downMatch++;
        }

        if (upMatch + downMatch + 1 >= 3) {
            matchingCells.Add(cell);
            return true;
        }

        matchingCells.Clear();
        return false;
    }

    public static void Swap(this Dictionary<Vector2Int, CellView> _cells, CellView c1, CellView c2, float animTime = 0f)
    {
        var bufferPlace = c1.GetPlace();
        c1.SetPlace(c2.GetPlace(), animTime);
        _cells[c1.GetPlace()] = c1;
        c2.SetPlace(bufferPlace, 0);
        _cells[c2.GetPlace()] = c2;
    }

    public static void GetRandomColorForCell(this Dictionary<Vector2Int, CellView> _cells, CellView cell)
    {
        var colorCandidatesForCell = new List<Color>(_colorsTemplate);
        bool hasAnyMatch = true;
        while (hasAnyMatch) {
            var result = colorCandidatesForCell[Random.Range(0, colorCandidatesForCell.Count)];
            cell.SetColor(result);
            colorCandidatesForCell.Remove(result);
            hasAnyMatch = _cells.HasAnyMatch(cell, out var cells);
        }
    }

    public static bool CheckMatchingFor(this Dictionary<Vector2Int, CellView> _cells, List<CellView> cellViews, out List<CellView> checkForMatching)
    {
        bool hasAnyMatch = false;
        checkForMatching = new List<CellView>();
        for (int i = 0; i < cellViews.Count; i++) {
            hasAnyMatch |= _cells.HasAnyMatch(cellViews[i], out var matchingCells);

            if (matchingCells.Count > 0) {
                MainEventBusHandler.DispachAddPoints((matchingCells.Count - 1) * 5);
                Vector2Int nextCellPlace;
                foreach (var cell in matchingCells) {
                    cell.SetColor(Color.white - Color.black);
                    nextCellPlace = cell.GetPlace();
                    while (true) {
                        nextCellPlace.x++;
                        if (_cells.TryGetValue(nextCellPlace, out var nextCell) == false || nextCell == null) {
                            break;
                        }

                        if (nextCell.IsEmpty()) {
                            continue;
                        }

                        _cells.Swap(nextCell, cell, LevelSettings.ANIM_FALL_TIME);
                        nextCellPlace = cell.GetPlace();
                        checkForMatching.Add(nextCell);
                    }
                }

                foreach (var cell in matchingCells) {
                    _cells.GetRandomColorForCell(cell);
                    nextCellPlace = cell.GetPlace();
                    nextCellPlace.x++;
                    cell.SetPlace(nextCellPlace, 0);
                    nextCellPlace.x--;
                    cell.SetPlace(nextCellPlace, LevelSettings.ANIM_FALL_TIME);
                }
            }
        }

        return hasAnyMatch;
    }

    public static bool HasPossibleMoves(this Dictionary<Vector2Int, CellView> _cells)
    {
        foreach (var cell in _cells.Values) {
            Vector2Int nextCellPlace = cell.GetPlace();
            while (true) {
                nextCellPlace.x++;
                if (_cells.TryGetValue(nextCellPlace, out var nextCell) == false || nextCell == null) {
                    break;
                }

                if (nextCell.IsEmpty()) {
                    continue;
                }

                SwapColor(nextCell, cell);
                if (_cells.HasAnyMatch(cell, out var matchingCells)) {
                    _cells.Swap(nextCell, cell, 0);
                    return true;
                }
                if (_cells.HasAnyMatch(nextCell, out matchingCells)) {
                    _cells.Swap(nextCell, cell, 0);
                    return true;
                }
                SwapColor(nextCell, cell);
            }
            nextCellPlace = cell.GetPlace();
            while (true) {
                nextCellPlace.y++;
                if (_cells.TryGetValue(nextCellPlace, out var nextCell) == false || nextCell == null) {
                    break;
                }
                if (nextCell.IsEmpty()) {
                    continue;
                }

                SwapColor(nextCell, cell);
                if (_cells.HasAnyMatch(cell, out var matchingCells)) {
                    _cells.Swap(nextCell, cell, 0);
                    return true;
                }
                if (_cells.HasAnyMatch(nextCell, out matchingCells)) {
                    _cells.Swap(nextCell, cell, 0);
                    return true;
                }
                SwapColor(nextCell, cell);
            }
        }

        Debug.LogError("NO POSSIBLE MOVE!!!");

        return false;
    }

    public static void SwapColor(CellView c1, CellView c2)
    {
        var color = c1.cellMainImage.color;
        c1.SetColor(c2.cellMainImage.color);
        c2.SetColor(color);
    }


    public static List<Vector2Int> GetEmptyCells()
    {
        List<Vector2Int> emptyCells = new List<Vector2Int>();
        for (int i = 0; i < LevelSettings.EMPTY_CELL_CNT; i++) {
            emptyCells.Add(new Vector2Int(Random.Range(0, LevelSettings.ROW_CNT), Random.Range(0, LevelSettings.ROW_CNT)));
        }

        return emptyCells;
    }
}
