﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CellView : MonoBehaviour, IPointerClickHandler, ICell
{
    public Image cellMainImage;
    public Image cellSelectedImage;
    public GameObject teleport;
    
    private RectTransform _myRectTransform;
    private Vector2Int _myPlace;
    private int? _colorHash = null;
    private bool _isEmpty;

    void Awake()
    {
        _myRectTransform = GetComponent<RectTransform>();
    }

    void Start()
    {
        SetSelected(false);
    }

    public Vector2Int GetPlace()
    {
        return _myPlace;
    }

    public int? GetColorAlias()
    {
        return _colorHash ?? (_colorHash = cellMainImage.color.GetHashCode());
    }

    public bool IsEmpty()
    {
        return _isEmpty;
    }

    public void SetPlace(Vector2Int place, float animTime)
    {
        var distance = _myPlace - place;
        _myPlace = place;
        place.x = _myPlace.y;
        place.y = -_myPlace.x;
        _myRectTransform.DOKill();
        if (animTime > 0)
            _myRectTransform.DOAnchorPos(place * (int)_myRectTransform.rect.width, animTime * distance.magnitude);
        else
            _myRectTransform.anchoredPosition = place * (int)_myRectTransform.rect.width;
        _myRectTransform.localScale = Vector3.one;
    }

    public void SetColor(Color color)
    {
        _isEmpty = color.Equals(LevelSettings.EMPTY_COLOR);
        cellMainImage.color = color;
        _colorHash = cellMainImage.color.GetHashCode();
        teleport.SetActive(IsEmpty());
    }

    public void SetSelected(bool selected)
    {
        cellSelectedImage.gameObject.SetActive(selected);
    }

    public void MoveTo(Vector2Int place, float animTime)
    {
        var distance = _myPlace - place;
        var newPlace = place;
        newPlace.x = place.y;
        newPlace.y = -place.x;
        _myRectTransform.DOKill();
        _myRectTransform.DOAnchorPos(newPlace * (int)_myRectTransform.rect.width, animTime * distance.magnitude);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (IsEmpty()) return;

        MainEventBusHandler.DispachCellSelected(this);
    }
}

public interface ICell
{
    Vector2Int GetPlace();
    int? GetColorAlias();
    bool IsEmpty();
    void SetPlace(Vector2Int place, float animTime);
    void SetColor(Color color);
    void SetSelected(bool selected);
}
