﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PointsView : MonoBehaviour
{
    public TextMeshProUGUI points;
    private int _totalPoints;

    void Start()
    {
        MainEventBusHandler.OnAddPoints += OnPointsChanged;
        MainEventBusHandler.OnRestartGame += OnRestartGame;
        OnPointsChanged(0);
    }

    void OnDestroy()
    {
        MainEventBusHandler.OnAddPoints -= OnPointsChanged;
        MainEventBusHandler.OnRestartGame -= OnRestartGame;
    }

    private void OnRestartGame()
    {
        _totalPoints = 0;
        OnPointsChanged(0);
    }

    private void OnPointsChanged(int addPoints)
    {
        _totalPoints += addPoints;
        points.text = $"Очки: {_totalPoints}";
    }
}
