﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class NotifView : MonoBehaviour
{
    public TextMeshProUGUI notif;
    void Start()
    {
        MainEventBusHandler.OnShowNotif += OnNotifShow;
        notif.alpha = 0;
    }

    private void OnNotifShow(string txt)
    {
        notif.text = txt;
        notif.alpha = 0;
        notif.DOKill();
        notif.DOFade(1, LevelSettings.NOTIF_ANIM_TIME).OnComplete(() =>
        {
            notif.DOFade(0, LevelSettings.NOTIF_ANIM_TIME).SetDelay(LevelSettings.NOTIF_LIFE_TIME);
        });
    }
}
