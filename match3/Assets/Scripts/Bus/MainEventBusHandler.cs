﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainEventBusHandler
{
    public static event Action<CellView> OnCellSelected = delegate(CellView view) {  };
    public static event Action<int> OnAddPoints = delegate(int points) {  };
    public static event Action OnRestartGame = delegate() {  };
    public static event Action<string> OnShowNotif = delegate(string notif) {  };
    
    public static void DispachShowNotif(string notif)
    {
        OnShowNotif?.Invoke(notif);
    }

    public static void DispachRestartGame()
    {
        OnRestartGame?.Invoke();
    }

    public static void DispachAddPoints(int points)
    {
        OnAddPoints?.Invoke(points);
    }

    public static void DispachCellSelected(CellView view)
    {
        OnCellSelected?.Invoke(view);
    }
}
